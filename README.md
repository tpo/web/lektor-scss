 Dart SASS compiler for lektor
=============================
SASS/SCSS compiler for [Lektor](https://getlektor.com) that compiles css from sass or scss.

This is a fork of [lektor-scss](https://github.com/chaos-bodensee/lektor-scss) using the dart sass compiler instead of libsass.

 How does it actually work?
----------------------------
 + It uses [dart sass](https://sass-lang.com/dart-sass)
 + It looks for ``.scss`` and ``.sass`` files *(ignores part files that begin with a underscore e.g. '_testfile.scss') and compiles them as part of the build process.*
 + It only rebuilds the css when it's needed (file changed, a file it imports changed or the config changed).
 + When starting the the development server it watches the files for changes in the background and rebuilds them when needed.

 Installation
-------------
This plugin doesn't have a package available on pypi. For now you can clone it into your project's `packages` directory.

 Usage
------
To enable the plugin, pass the ``scss`` flag when starting the development
server or when running a build:
```bash
# build and compile css from scss
lektor build -f scss

# edit site with new generated css
lektor server -f scss
```

 Configuration
-------------
The Plugin has the following settings you can adjust to your needs:

| parameter     | default value          | description                                                                                                                                                                      |   |
|---------------|------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---|
| sass_binary   | output of `which sass` | an absolute path to your system's dart sass binary, by default tries to find a binary on your `$PATH`                                                                            |   |
| source_dir    | assets/scss/           | the directory in which the plugin searchs for sass files (subdirectories are included)                                                                                           |   |
| output_dir    | assets/css/            | the directory the compiled css files get place at                                                                                                                                |   |
| output_style  | compressed             | coding style of the compiled result. choose one of: 'nested', 'expanded', 'compact', 'compressed'                                                                                |   |
| source_map    | False                  | whether to generate source maps                                                                                                                                                  |   |
| include_paths |                        | If you want to include SASS libraries from a different directory, libsass's compile function has a parameter called `include_paths` to add those directories to the search path. |   |

An example file with the default config can be found at ``configs/scss.ini``. For every parameter that is not specified in the config file the default value is used by the plugin.

 Development
-------------
To test and/or develop on this plugin in your running lektor installation, simply place it in the ``packages/`` Folder and have a look at the [Lektor Doku](https://www.getlektor.com/docs/plugins/dev/)

<!-- How to add to pypi: https://packaging.python.org/tutorials/packaging-projects/ -->
<!-- Python RELEASEING moved to github action -->
<!-- You have to edit the version number in README and setup.py manually -->
